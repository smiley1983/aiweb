from aiweb_tools.games import engine
from aiweb_tools.games.uttt_game import uttt
from aiweb_tools.games import Game
from aiweb_tools import games

import os.path

from aiweb_tools import config

test_map_path = config.map_path + "Tron/test_map.map"

class Uttt(games.Game):

	max_players = 2

	def __init__(self, opts, players, player_names, map_path="", teams=[]):
		#self.opts = opts
		self.gamename = "Uttt"
		if map_path == "":
			self.map_path = test_map_path
			turns = 10
		else:
			self.map_path = map_path
			turns = 1000
		self.opts = {
			'turns':82,	
			'timebank': 10000,
			'loadtime': 10000,
			'turntime': 500,
			'time_per_move': 500,
			'player_names' :player_names,
		}
		self.players = players
		self.player_names = player_names
		#self.teams = teams

	def run_game(self):
		#map_path = os.path.join(maps_path, temp_map)
		print("map path: " + str(self.map_path))
		with open(self.map_path) as fo:
			map_text = "".join(fo.readlines())
		self.opts['map'] = map_text
		game = uttt.Uttt(self.opts)
		game_result = engine.run_game(game, self.players, self.opts)
		game_result['playernames'] = self.player_names
		if 'replaydata' in game_result:
			game_result['replaydata']['playernames'] = self.player_names
		print(game_result)
		return game_result

 
Game = Uttt
